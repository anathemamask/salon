/* eslint-disable no-unused-vars */
import SalonOverlay from "./apps/SalonOverlay.js";
import SalonNavi from "./apps/SalonNavi.js";
import SalonSpy from "./apps/SalonSpy.js";
import SalonWhisper from "./apps/SalonWhisper.js";
import { SALONsettings, SALONoptions } from "./settings/settings.js";

(() => {})();

Hooks.once("init", async () => {
//  CONFIG.debug.hooks = true
  SALONsettings.registerSettings();
});

function _updateTimestamps(msgs) {
  let stamps = msgs.reduce((acc, val) => {
    acc[val._id] = val.data.timestamp;
    return acc;
  }, {});
  if ($(".SIM-chat-lane").length !== 0) {
    $(".SIM-chat-lane")
      .children()
      .each((i, li) => {
        let id = li.getAttribute("data-message-id"),
          stamp = stamps[id];
        if (!stamp) return;
        li.querySelector(".message-timestamp").textContent = timeSince(stamp);
      });
  }
}

async function _processWhispers(array, sender, recipient) {
  let currentUser = game.user.id;

  let msgs = [];
  array.forEach((x) => {
    if (
      (x.user.data._id === sender && x.data.whisper[0] === recipient) ||
      (x.user.data._id === recipient && x.data.whisper[0] === sender)
    ) {
      msgs.push(x);
    }
  });
  return Promise.resolve(msgs);
}

Hooks.once("canvasReady", async () => {

  
  SalonOverlay.init();
  if (ui.salonOverlay) ui.salonOverlay.render();
});

Hooks.on("ready", () => {});

Hooks.on("renderSalonOverlay", async () => {

  SalonNavi.init();
  let patchFuncStr = "";
  const rolllane = $("#salon-roll-lane");

  const fvttrollControls = $("#chat-controls");

  const fvttchatEntry = $("#chat-form");

  const fvttchatLog = $("#chat");
  const chatLog = $("#chat-log");
  const chatlane = $("#salon-chat-lane");
  const rollMessages = $(".type5");
  const otherMessages = $(".type0");
  const rollLog = $(".roll-log");
  $(".type5").removeClass("hardHide");
  $(".type0").removeClass("hardHide");
  const fvttnav = $("#navigation");

  chatlane.append(fvttchatLog);
  chatLog.append(rollLog);
  fvttchatLog
    .removeClass("sidebar-tab tab active flexcol")
    .addClass("chat-old");
  chatlane.append(fvttchatEntry);
  $(".ooc").append($("#oocNotification"));
  $("#rollsNotification").detach();
  rolllane.append(fvttrollControls);
  fvttchatEntry.addClass("salon-chat-entry");
  $("a[data-tab=combat]")[0].click();
  $("a[data-tab=ooc]")[0].click();
  $("a[data-tab=ic]")[0].click();
  rollMessages.show();
  $("a[data-tab=rolls]").hide();
  $("#chat-message").focus();
  fvttchatEntry.addClass("salon-chat-entry");
  $("a[data-tab=combat]")[0].click();
  $("a[data-tab=ic]")[0].click();
  $("a[data-tab=ooc]")[0].click();
  $("a[data-tab=ic]")[0].click();
  $("#chat-message").focus();
  $(".roll-log").append($(".type5"));
  $(".roll-log").append($(".type0"));
  $("#salon-overlay > header > a").detach();

});

Hooks.on("renderSidebarTab", () => {
  
  setTimeout(() => {
    $(".roll-log").append($(".type5"));
    $(".roll-log").append($(".type0"));
    $(".type5").css("display", "list-item");
    $(".type0").css("display", "list-item");
  }, 10);
  $(".roll-log").scrollTop($(".roll-log").prop("scrollHeight"));
});

Hooks.once("renderSalonOverlay", () => {
  game.salon.typingNotifier = new SalonSpy();
});

Hooks.on("renderSceneNavigation", (sceneNav) => {
  let viewedScene = sceneNav.scenes.find(x => x.isView);

  if (viewedScene === undefined) {
    Scene.create({"name": "New Scene", "backgroundColor": "#000500", "gridColor": "#FFFFFF","width": 2000, "height": 2000, "active": true})
  };
  //Reduce the scale of the entire UI to make it more comfortable for people
  //but only on small screens
  if ($(window).innerWidth() <= 1400 || $(window).innerHeight() <= 900) {
    $("#notifications").css({
      zoom: "60%",
    });
    $("#logo").css({
      zoom: "60%",
    });
    $("#loading").css({
      zoom: "60%",
    });
    $("#controls").css({
      zoom: "60%",
    });
    $("#players").css({
      zoom: "60%",
    });
    $("#hotbar").css({
      zoom: "70%",
    });
    $("#pause").css({
      zoom: "60%",
    });
    $("#sidebar").css({
      zoom: "75%",
    });
    $("#navigation").css({
      zoom: "60%",
      right: "330px",
    });
    $("#salon-roll-lane").css({
      zoom: "90%",
      width: "220px",
      "font-size": "1.1rem",
    });
    $("#chat-controls").css({
      zoom: "75%",
    });
    $("#salon-chat-bounding-box").css({
      zoom: "90%",
    });
    $(".roll-log").css({
      zoom: "90%",
    });
    $("#vino-chat-lane").css({
      zoom: "80%",
    });
  }
});


Hooks.on("createChatMessage", async (msg) => {

  let currentUser = game.user.id;

  
  const whisps = await game.messages.entities.filter((m) => m.data.type === 4);
  _updateTimestamps(whisps);
  if (
    msg.data.type === 4 &&
    (msg.data.whisper.includes(currentUser) || msg.data.user === currentUser)
  ) {
    let sender = msg.data.user;
    let recipient = msg.data.whisper[0];
    let otherDude;
    let msgs = await _processWhispers(whisps, sender, recipient);
    let myName = game.users.get(currentUser).name;
    let otheruser = game.users.get(recipient).data._id;

    if (currentUser !== otheruser) {
      otherDude = game.users.get(recipient).name;
    } else {
      otherDude = game.users.get(sender).name;
    }

    let existingWindow = $(`#SIM-${myName}-${otherDude}`).length;
    if (existingWindow === 0 || undefined) {
      new SalonWhisper(msgs, sender, recipient).render(true);
      setTimeout(() => {
        $(".SIM-chat-lane").scrollTop($(".SIM-chat-lane").prop("scrollHeight"));
      }, 100);
    } else {
      let newMsg = msg;
      let newMsgID = msg.data._id;
      let alias = msg.alias;
      let timestamp = timeSince(msg.data.timestamp);
      let msgTo = game.users.get(recipient).name;
      let msgContent = msg.data.content;
      let msgHtml = `
  <li class="chat-message message flexcol" data-message-id="${newMsgID}">
        <header class="message-header flexrow">
            <h4 class="message-sender">${alias}</h4>
            <span class="message-metadata">
                <time class="message-timestamp">${timestamp}</time>

            </span>
           
          
        </header>
        <div class="message-content">
            ${msgContent}
        </div>
    </li>
  `;
      $(`#SIM-${myName}-${otherDude} > section > div > div`).append(msgHtml);
      setTimeout(() => {
        $(".SIM-chat-lane").scrollTop($(".SIM-chat-lane").prop("scrollHeight"));
      }, 0);
    }
  }
  if (msg.data.content === "SALONDeleteme") {
    let deleteID = msg.data._id;
    setTimeout(() => {
      ChatMessage.delete(`${deleteID}`);
    }, 100);
  }
});
/*
Hooks.on("chatMessage", async (msg) => {
  console.log(msg.data.content)
 if (msg.data.content[0] = "/") {
   console.log("THERES A SLASH IN THIS")
   return false
 }
});

*/
Hooks.on("deleteChatMessage", async (msg) => {
  let deleteID = msg.data._id;

  $(".SIM-chat-lane").find(`[data-message-id='${deleteID}']`).detach();
});

Hooks.on("renderChatMessage", async (msg) => {
  let msgId = msg.data._id
  let msgUser = msg.data.user
  let name = game.users.get(msgUser).data.name
  let renderedHtml = await renderTemplate(
    "modules/salon/templates/salon-namestamp.html",
    {
      ID: msgId,
    }
  );
  
  let appendUser = `<span class="salonUser">(${name})</span>`
  $(`[data-message-id^="${msgId}"] > header > span`).prepend(appendUser)
});

const ogPostOne = ChatLog.prototype.postOne;

ChatLog.prototype.postOne = async function (...args) {
  // Call original function
  await ogPostOne.call(this, ...args);
  $(".roll-log").append($(".type5"));
  $(".roll-log").append($(".type0"));
  $(".roll-log").scrollTop($(".roll-log").prop("scrollHeight"));
};
