# Salon - A Chat-centric UI replacement for FVTT
This module attempts to streamline the roleplaying process in FVTT for text-based roleplayers, by implementing UI changes and bringing some QOL changes for those used to playing their games via Discord or IRC.


![Salon Preview Image](/assets/Salon-Preview-Screenshot.png)
![Salon Preview Video](/assets/Salon-Release-Preview-2020-sept.mp4)

## Dependencies
Requires tabbed-chatlog and wouldn't be possible without it.
Requires an internet connection for interact.js to function (temporarily), looking to resolve this in the future.

## Features
#### New, Wider Chatlog
- Divides the chatlog into a popped out frame separate from the sidebar, giving it a display across the top of the FVTT interface. 
- Splits rolls from the chatlog into its own separate scrolling display which can always be found in the bottom right hand corner of the screen.
- Fully draggable and resizeable
- Cannot be closed, but can be minimized, double-click the top menu to shrink and hide chat!
#### Salon Instant Messenger
- If an instant message is sent or received, opens a small private window to log the messages
- The SIM window allows convenient sending and receiving of messages with the individual that message contains
- Right click a username in the chatlog to open an instant message window quickly and conveniently, and save typing their username!
- Does not support 0.7.x multi-user whispering at this time
#### Significantly reshapes the UI
- Shifts the scene navigation bar to the bottom of the screen to accommodate the moving of the chat UI. 
- Shrinks the width (slightly) and height (significantly) of the Sidebar in order to provide more screenspace.
- Allows for the sidebar to be veritically resized by dragging its bottom edge. 
#### Allows Editing of Posted Chat Messages
- Posted a very descriptive action for a character only to realise you made a grammar error? Simply double-click the message to bring up a simple plain text editor and make the correction you need to make. (Only works if you are the author of the message or the GM, does not work on rolls.)
#### Shows when others are typing
- Provides a nice clean notification to indicate when you or your players are actively typing
#### Integrated support
- Using MEME to augment chat entry with markdown is highly recommended!
- Out of the box support for ViNo
- Because Salon works by moving the original FVTT chat log and its messages, only requires minimal localization for the editor window.





## Roadmap
Salon plans to bring a number of additional features to its UI suite for those who run their games entirely through text.   

**Planned Features**
- A suite of /commands to augment the robust chat features of FVTT
	- /as and /emas support
	- /nick to temporarily change the name all of your posts will appear from
	- /desc to post extended, stylized descriptions
	- /zalgo to generate zalgo text artefacting (for those horror game players out there)
- Log chat on a per-scene basis to journal entries that automatically update whenever a new post is made to that scene

## Known Issues
This module will very likely not play well with:
- Any other module or system that significantly modifies the UI (results may vary.)

## License
Open. If you feel you can significantly improve this module please by all means do so. I also take suggestions for ways to improve my logic and flow, but given my level of knowledge with JS (spoiler alert: it's bad) I may straight out tell you no. 

This code isn't the worst I've written- but like that PC in the corner of your office that still runs Windows 98: it shouldn't work, no one knows why it's working, and the only person who uses it couldn't tell you why they like it.
