export const SALONoptions = {
DISABLE_CHAT_RECALL: "disableChatRecall",
NOTIFY_TYPING: "typingNotifier"
}


export class SALONsettings {
	static registerSettings() {
        
        game.settings.register("salon", SALONoptions.DISABLE_CHAT_RECALL, {
			name: "salon.disable-chat-recall-s",
			hint: "salon.disable-chat-recall-l",
			scope: "client",
			config: true,
			default: true,
			type: Boolean,
			onChange: disableChatRecall => window.location.reload()
		});
		
		game.settings.register("salon", SALONoptions.NOTIFY_TYPING, {
			name: "salon.notify-typing-s",
			hint: "salon.notify-typing-l",
			scope: "world",
			config: true,
			default: true,
			type: Boolean,
			onChange: spyTyping => window.location.reload()
		});
    }
    static getSetting(option) {
		return game.settings.get("salon", option);
	}
}
