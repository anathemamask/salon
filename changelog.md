## Changes
Version 0.1.6 - Corrected an issue which caused the Salon overlay to fail to render due to an upstream change in interact.js. Now using interact.js via CDN, this means Salon requiers an internet connection for use or it will fail.

Version 0.1.5 - Fixed some CSS related to private message windows not showing text color in the editor, added a placeholder and border to make the message send box more visible. Also added some CSS corrections for the <> wrapping on message sender to support narrator tools and stop showing empties. Added some CSS for Dicetay. It was not fine. Still isn't. Dice tray's html selector is incompatible with salon. 

Version 0.1.4 - Fix salon not correctly rendering if there's no active scene through the worst possible way: creating an active scene. Also support Polyglot. Wait- what happens with Dice Tray? Ehhhhhhh it's probably fine. 

Version 0.1.3 - Fix right click to open whisper, which no longer shows the 'reveal' context menu. Corrected css for entity links and the chat message edit box so it isn't blindingly unreadable. Use jquery and stick usernames on chat messages by abusing rendertemplate.

Version 0.1.2 - I don't remember what i changed but it definitely isn't important

Version 0.1.1 - ~~Put back the code you overzealously deleted you asshole~~ Correct minor script error which resulted in typing notifications no longer functioning.

Version 0.1.1 - ~~Fix a bunch of garbage that came from me not checking what happens if you don't have Cautious Gamemaster's Pack Installed~~ update for 0.7.4, definitely not because i screwed something up

version 0.1.0 - Release version! Removes a bunch of absolute garbage from the code and adds a bunch of different, equally garbage code. Complete rebuild of the overlay application as a draggable and resizable window which cannot be closed. Additionally, the sidebar can now be drag-resized. Timestamps in the instant messenger windows now update whenever a message is created anywhere.  Confirmed working in 0.7.3.

version 0.0.9 - final pre-release, adds the entire framework for instant messenger windows on whispers

version 0.0.8 - add support for "is typing" notifications harvested from Cautious Game Master's Pack

version 0.0.7 - just some general tweaks, nothing serious

version 0.0.6 - Laptops with tiny screens suck, abuse the UI to fit better.

version 0.0.5 - Prerelease 
- The part where I find out that I left all those debug hooks on and all those console.logs scattered in the code.