/* eslint-disable no-useless-return */
/* eslint-disable class-methods-use-this */
import SalonEditor from './SalonEditor.js';
import SalonSpy from './SalonSpy.js';
import constants from '../constants.js';
import interact from 'https://cdn.interactjs.io/v1.9.20/interactjs/index.js'
import { SALONsettings, SALONoptions } from "../settings/settings.js";
const PACKET_HEADER = {
	OTHER: 0,
	TYPING_MESSAGE: 1,
	TYPING_END: 2
}
export default class SalonOverlay extends Application {
  /* const app = ;
    static app; */

  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      id: 'salon-overlay',
      template: `${constants.modulePath}/templates/salon-frame.html`,
      top: 0,
      left: '20%',
      scrollY: ["#chat-log"],
      classses: ['.salon-chat-bounding-box'],
      popOut: true,
      resizable: true,
      draggable: true
    });
  }
  close() { new MainMenu().render(true);}

  getData(options = {}) {
    options = super.getData(options);
    return options;
  }

  static init() {
    const instance = new this();
    ui.salonOverlay = instance;

    instance.render(true);
  }

  _salonScrollFocus() {
    setTimeout(() => { $('#chat-log').scrollTop($('#chat-log').prop('scrollHeight')); }, 10000);
  }

  _onToggleNav(event) {
    event.preventDefault();
  }

  activateListeners(html) {

    game.salon = {};
    if (SALONsettings.getSetting(SALONoptions.NOTIFY_TYPING)) {
      let cgpActive
  
      if (typeof game.modules.get("CautiousGamemastersPack") !== "undefined") {
      cgpActive = game.modules.get("CautiousGamemastersPack").active
      }
      else {
        cgpActive = false;
      }
  if (cgpActive != true) {
 
      $( "textarea" ).focus((ev) => {
      
      if(ev.target.form.id === 'chat-form') {
        $('#chat-form').keydown((ev) => { 
          let data = {
            header: PACKET_HEADER.TYPING_MESSAGE,
			      user: game.user.id
          }
          game.socket.emit('module.salon', data)
       
        })
      }
      
    })
  }
    
    }
    
    let sideBar = $("#sidebar")
    interact('#sidebar').resizable({
      edges:{
        left: false,
        right: false,
        top: false,
        bottom: true
      },
      
      listeners: {
        move (event) {
          var target = event.target
          var x = (parseFloat(target.getAttribute('data-x')) || 0)
          var y = (parseFloat(target.getAttribute('data-y')) || 0)
  
          // update the element's style
          target.style.width = event.rect.width + 'px'
          target.style.height = event.rect.height + 'px'
  
          // translate when resizing from top or left edges
          x += event.deltaRect.left
          y += event.deltaRect.top

        }
      },
      modifiers: [
        // keep the edges inside the parent
        interact.modifiers.restrictEdges({
          outer: 'parent'
        }),
  
        // minimum size
        interact.modifiers.restrictSize({
          min: { width: 100, height: 50 }
        })
      ],
  
      inertia: true
    
    })
    super.activateListeners(html);
    
    $(window).resize(()=> {
      if (($(window).innerWidth() <= 1400) || ($(window).innerHeight() <= 900)) {
        $('#notifications').css({zoom: '60%'})
        $('#logo').css({zoom: '60%'})
        $('#loading').css({zoom: '60%'})
        $('#controls').css({zoom: '60%'})
        $('#players').css({zoom: '60%'})
        $('#hotbar').css({zoom: '70%'})
        $('#pause').css({zoom: '60%'})
        $('#sidebar').css({zoom: '75%'})
        $('#navigation').css({zoom: '60%', right: '330px'})
        $('#salon-roll-lane').css({zoom: '90%', width: '220px', "font-size": "1.1rem"})
        $('#chat-controls').css({zoom: '75%'})
        $('#salon-chat-bounding-box').css({zoom: '90%'})
        $('.roll-log').css({zoom: '90%'})
        $('#vino-chat-lane').css({zoom: '80%'})
    }else{
      $('#notifications').css({zoom: ''})
      $('#logo').css({zoom: ''})
      $('#loading').css({zoom: ''})
      $('#controls').css({zoom: ''})
      $('#players').css({zoom: ''})
      $('#hotbar').css({zoom: ''})
      $('#pause').css({zoom: ''})
      $('#sidebar').css({zoom: ''})
      $('#navigation').css({zoom: '', right: ''})
      $('#salon-roll-lane').css({zoom: '', width: ''})
      $('#chat-controls').css({zoom: ''})
      $('#salon-chat-bounding-box').css({zoom: ''})
      $('.roll-log').css({zoom: ''})
      
    }
      })  
    
    const fvttchatLog = $('#chat');
    const chatlane = $('#salon-chat-lane');
    const boundingBox = $('#salon-chat-bounding-box');

    let expandcheck = false;
    let collapsecheck = false;
    html.find('#nav-toggle').click(this._onToggleNav.bind(this));

    fvttchatLog.delegate('.message', 'dblclick', async (ev) => {
      ev.preventDefault();

      const msgID = ev.currentTarget.dataset.messageId;
      const origMSG = game.messages.entities.find((m) => m.data._id === `${msgID}`);
      const chatMSG = ChatMessage.collection.get(`${msgID}`).data.content;
      if ((origMSG.owner === true || origMSG.isAuthor === true) && origMSG.isRoll === false) {
        const salonChatMsg = await SalonEditor.create(chatMSG, false);
        if (salonChatMsg === '') {
          console.log('Salon - Rejecting changes from edited message, you should not try to delete messages this way.');
          ui.notifications.error('Rejected changes - you should not try to delete a message this way.');
          return;
        } else {
          return origMSG.update(
            {
              content: `${salonChatMsg}`,
            },
          ).then((newMsg) => {
            ui.chat.updateMessage(newMsg);
          });
        }
      } else {
        ui.notifications.warn('You cannot edit this message.');
      }
    });
    
    $(".message-sender").contextmenu((ev) => {
      ev.preventDefault();
      ev.stopPropagation();
      let targetID
      let name = ev.currentTarget.outerText
      try{targetID = game.users.getName(`${name}`).data._id}
      catch{return}

      ChatMessage.create({
        user: game.user._id,
        type: 4,
        whisper: [`${targetID}`],
        content: "SALONDeleteme"
    });
    
  })
}
}
