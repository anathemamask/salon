export default class SalonNavi extends SceneNavigation {
  static init() {
    const instance = new this();
    instance.render(true);
  }

  /**
   * Expand the SceneNavigation menu, sliding it down if it is currently collapsed
   */
  _expand() {
    if (!this._collapsed) return true;
    const nav = this.element;
    const icon = nav.find('#nav-toggle i.fas');
    const ul = nav.children('#scene-list');

    return new Promise((resolve) => {
      ul.slideDown({
        duration: 200,
        done: () => {
          nav.removeClass('collapsed');
          icon.removeClass('fa-caret-down').addClass('fa-caret-up');
          this._collapsed = false;
        },
        start: () => {
          ul.css({ display: 'flex' });
        },
      });
      resolve(true);
      Hooks.callAll('collapseSceneNavigation', this, this._collapsed);
    });
  }
  /* -------------------------------------------- */

  /**
   * Collapse the SceneNavigation menu, sliding it up if it is currently expanded
   */
  async _collapse() {
    if (this._collapsed) return true;
    const nav = this.element;
    const icon = nav.find('#nav-toggle i.fas');
    const ul = nav.children('#scene-list');
    return new Promise((resolve) => {
      ul.slideUp(200, () => {
        nav.addClass('collapsed');

        icon.removeClass('fa-caret-up').addClass('fa-caret-down');
        this._collapsed = true;
      });
      resolve(true);
      Hooks.callAll('collapseSceneNavigation', this, this._collapsed);
    });
  }

  activateListeners(html) {
    html.find('#nav-toggle').click(this._onToggleNav.bind(this));
    super.activateListeners(html);
  }

  _onToggleNav(event) {
    event.preventDefault();
    if (this._collapsed) this._expand();
    else this._collapse();
  }
}
