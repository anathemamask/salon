/* eslint-disable prefer-const */
export default class SalonEditor extends Dialog {
  constructor(chatMsg, dialogData = {}, options = {}) {
    super(dialogData, options);
    this.options.classes = ["farrago-modern", "editor"];

  }

  static async create(chatData, isEdit = true, newMsg = "") {

    let chatMsg = "";

    let html = await renderTemplate(
      "modules/salon/templates/salon-editor.html",
      {
        classes:["salon-editor"],
        chatData,
        newMsg,
        chatMsg: chatData,
        isGM: game.user.isGM,
        isEdit,
      }
    );
    return new Promise((resolve) => {
     
      new Dialog({
        title: "Edit Chat Message",
        chatData,
        chatMsg,
        content: html,
        buttons: {
          submit: {
            label: "Submit",
            callback: () => {
              newMsg = $(".salon-editor-area")[0].value;
            },
          },
          cancel: {
            label: "Cancel",
            callback: () => {
              newMsg = "";
            },
          },
        },
        close: () => {
          if (newMsg === (null || undefined)) {
            newMsg = "";
            resolve(newMsg);
          } else {
            resolve(newMsg);
          }
        },
      }).render(true);
    });
  }
}
