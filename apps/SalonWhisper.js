import constants from '../constants.js';
let myName
let otherDude 
export default class SalonIM extends Application { 
    
    constructor(msgs, sender, recipient) {
       let currentUser = game.user.id 
       let myName = game.users.get(currentUser).name
       let otheruser = game.users.get(recipient).data._id
       if (currentUser !== otheruser) {
        otherDude = game.users.get(recipient).name
      } else {
        otherDude = game.users.get(sender).name
      }
        let whispID = game.i18n.format("salon.whisperID", { user1: myName, user2: otherDude })
        let whispTitle = game.i18n.format("salon.whisperTitle", {user1: otherDude})
        super({ id: whispID, title: whispTitle});
        this.sender = currentUser
        this.whisperTgt = game.users.getName(otherDude)
        this.currentUser = currentUser
        this.myName = myName
        this.otherDude = otherDude
        this.whispID = whispID
        this.chatentry = ""
        this.msgs = msgs
        
    }
    static get defaultOptions() {
            super.constructor()
            return mergeObject(super.defaultOptions, {
            title: game.i18n.localize("salon.whisperTitle"),
            classes: ["sim-window"],
            template: `${constants.modulePath}/templates/salon-im.html`,
            popOut: true,
            resizable: true,
            closeOnSubmit: false,
            submitOnChange: true
        })
    }
    async getData(options) {
        return mergeObject({
               msgs: this.msgs,
               msg: this.chatentry,
               myName: this.myName,
               otherDude: this.otherDude
            },
        );
        }
    activateListeners(html) {
               super.activateListeners(html)
        $(".salon-editor-area").keydown((ev) => {
            let formdata = this.msgs
            if (ev.key !== 'Enter') return;
            ev.preventDefault(); ev.stopPropagation();
            this.send(ev)
       })
    }
    
    send(ev) {
    let currentMSG = ev.target.value

    if (currentMSG === '' || undefined || null) return;
    let currentUser = this.currentUser
    let myName = this.myName
    let otherDude = this.otherDude
    let chatData = {
        type: 4,
        user: this.sender,
        content: currentMSG,
        whisper: [this.whisperTgt],
        sound: "sounds/notify.wav"
    }

    ChatMessage.create(chatData)
    if (this.currentUser === `${chatData.user}`) {
    let textEntry = $(`#SIM-${myName}-${otherDude} > section > div > textarea`)
    textEntry.val('')

    setTimeout(() => {$('.SIM-chat-lane').scrollTop($('.SIM-chat-lane').prop('scrollHeight'))}, 100)
    let chatEntry = $(`#SIM-${myName}-${otherDude} > section > div > textarea`)

    setTimeout(() => {chatEntry.focus()},150)
    }
  
}
}