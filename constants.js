const constants = {
  modulePath: 'modules/salon',
  moduleName: 'salon',
  moduleLabel: 'Salon - A UI suite for text-based games',

};
export default constants;
